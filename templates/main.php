<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $args['tracking_id']; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?php echo $args['tracking_id']; ?>');
  <?php if($args['google_ads_id']): ?>
    gtag('config', '<?php echo $args['google_ads_id']; ?>');
  <?php endif; ?>
</script>
