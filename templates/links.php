<script>
    const outbound_links = document.querySelectorAll("a")

    outbound_links.forEach( ( a ) => {
        
        let href = a.getAttribute('href')

        if(href.toLowerCase().startsWith('http')) {
            let url = new URL(href)
            if(url.hostname !== window.location.host) {
                a.setAttribute('target','_blank')
                a.addEventListener('click', (event) => {
                    gtag('event','click',{'event_category':'outbound','event_label':url})
                })
            }
        }
    } )

</script>