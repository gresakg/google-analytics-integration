<?php
/**
 * Plugin Name:     Google Analytics Integration
 * Plugin URI:      https://gresak.net
 * Description:     Simple plugin for integrating google analytics.
 * Author:          Gregor Grešak
 * Author URI:      https://gresak.net
 * Text Domain:     google-analytics-integration
 * Domain Path:     /languages
 * Version:         2.2.0
 *
 * @package         Google_Analytics_Integration
 */

require_once("vendor/autoload.php");

$container = Grit\Container::getInstance();

$container['gritg.gai'] = new Gritg\GAIntegration(__FILE__);
