<?php 

namespace Gritg;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Helper\Helper;
use Grit\Plugin;

class GAIntegration extends Plugin 
{

	protected $tracking_id;

	protected $codes = [];

	protected $cf7string;

	protected $ga_tracking_id;

	protected $update_url = "https://demo.gresak.net/google-analytics-integration/index.php";

	protected function init()
	{
		$this->cf7string = __('Contact form','google-analytics-integration');
	}

	protected function setFilters()
	{
		add_action('carbon_fields_register_fields',[$this,"set_option_page"],10);
		add_action('carbon_fields_register_fields',[$this,'set_tracking_id'],20);
		add_action('wp_head',[$this,'print_codes'],1);
		add_action('wp_body_open',[$this,'print_codes_after_body_tag'],1);
		add_action('wp_footer', [$this, 'outbound_links_tracking']);
		add_action('admin_notices',[$this, 'tracking_id_missing_notice']);
	}

	public function set_option_page()
	{
		$fields = [
			Field::make("html",'title',__("title-ga"))->set_html(sprintf('<h1>%s</h1>',__("Google Analytics", "google-analytics-integration"))),
			Field::make('checkbox','gai_enable_ga',__("Enable google analytics",'google-analytics-integration'))->set_default_value(true)->set_width(20),
			Field::make("text","ga_tracking_id", __("Google Analytics tracking ID",'google-analytics-integration'))->set_width(40),
			Field::make("text","google_ads_id", __("Google Ads tracking ID (optional)",'google-analytics-integration'))->set_width(40),
			Field::make("checkbox","ggga_track_outbound", __("Track clicks on outbound links as events", 'google-analytics-integration'))->set_width(33)
		];
		
		if(defined("WPCF7_VERSION")) {
			$fields[] = Field::make('checkbox',"gai_track_cf7",__("Track CF7 form submissions",'google-analytics-integration'))->set_width(33);
		}

		if( class_exists('woocommerce')) {
			$fields[] = Field::make('checkbox',"gai_track_woocommerce_order_recieced",__("Track recieved WooCommerce orders",'google-analytics-integration'))->set_width(33);
			$fields[] = Field::make('text', 'conversion_id',__( 'Conversion ID from your Google Ads','google-analytics-integration' ));
		}

		// gtm
		$fields[] = Field::make("html",'title-gtm',__("title-gtm"))->set_html(sprintf('<h1>%s</h1>',__("Google Tag Manager", "google-analytics-integration")));
		$fields[] = Field::make('checkbox','gai_enable_gtm',__("Enable Google tag manager",'google-analytics-integration'))->set_default_value(false)->set_width(20);
		$fields[] = Field::make("text","gtm_id", __("Google tag manager ID",'google-analytics-integration'))->set_width(40);

		// umami
		$fields[] = Field::make("html",'title-umami',__("title-umami"))->set_html(sprintf('<h1>%s</h1>',__("Umami", "google-analytics-integration")));
		$fields[] = Field::make('checkbox','gai_enable_umami',__("Enable Umami",'google-analytics-integration'))->set_width(20);
		$fields[] = Field::make('text','gai_umami_script_url',__('Umami script url','google-analytics-integration'))->set_width(40);
		$fields[] = Field::make('text', 'gai_umami_site_id',__("Site ID",'google-analytics-integration'))->set_width(40);

		$fields[] = Field::make("html",'title-facebook',__("title"))->set_html(sprintf('<h1>%s</h1>',__("Facebook pixel", "google-analytics-integration")));
		$fields[] = Field::make('checkbox','gai_enable_facebook_pixel',__("Enable facebook pixel",'google-analytics-integration'))->set_width(25);
		$fields[] = Field::make('text',"gai_facebook_pixel", __("Facebook Pixel ID",'google-analytics-integration'))->set_width(75);

		if( function_exists("cn_cookies_accepted")) {
			$fields[] = Field::make("html",'title-consent',__("title"))->set_html(sprintf('<h1>%s</h1>',__("Consent management", "google-analytics-integration")));
			$fields[] = Field::make('checkbox',"gai_consent_integration", __("Block scripts untill cookies are accepted. (works with
			Cookie Notice & Compliance for GDPR / CCPA plugin)",'google-analytics-integration'));
		}

		Container::make(
			"theme_options", __("GA Integration",'google-analytics-integration'))
		->set_page_parent('options-general.php')
		->add_fields($fields);
	}

	/**
	 * Sets the google analytics tracking id. If the option is not set, checks
	 * theme costumizer mod for compatibility reasons
	 */
	public function set_tracking_id()
	{
		$this->tracking_id = Helper::get_theme_option('ga_tracking_id');
		if(empty($this->tracking_id)) {
			if ($this->tracking_id = get_theme_mod('tracking_id')) add_option('ga_tracking_id',$this->tracking_id);
		} 
	}

	public function print_codes()
	{

		$this->print_gtm_code();

		$this->print_google_analytics_code();
		
		$this->print_facebook_pixel();

		$this->print_umami();
		
	}

	public function print_codes_after_body_tag()
	{
		$this->print_noscript_gtm_code();
	}

	public function outbound_links_tracking()
	{
		if( Helper::get_theme_option('ggga_track_outbound')) {
			$this->load_template("links");
		}
	}

	public function tracking_id_missing_notice()
	{
		if( empty($this->tracking_id) && Helper::get_theme_option('gai_enable_ga') ) {
			$message = sprintf( __("Google Analytics measurment ID is missing! Please set it in <a href='%soptions-general.php?page=crb_carbon_fields_container_ga_integration.php'>Settings > GAIntegration</a> ",'google-analytics-integration'),get_admin_url());
			$class = "notice notice-error ";
			printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
		}
	}

	protected function print_gtm_code()
	{
		if( ! Helper::get_theme_option("gai_enable_gtm") ) return;

		$data['gtm_id'] = Helper::get_theme_option('gtm_id');

		$this->load_template('gtm', $data);
	}

	protected function print_noscript_gtm_code()
	{
		if( ! Helper::get_theme_option("gai_enable_gtm") ) return;

		$data['gtm_id'] = Helper::get_theme_option('gtm_id');

		$this->load_template('noscriptgtm', $data);
	}

	protected function print_google_analytics_code()
	{
		
		if( !$this->check_consent() ) return;

		if( ! Helper::get_theme_option('gai_enable_ga') ) return;

		$this->set_cf7string();

		$google_ads_id = $this->get_google_ads_id();
		$data = [
			'tracking_id' 	=> $this->tracking_id,
			'google_ads_id'	=> $google_ads_id
		];
		
		$this->load_template('main', $data);
		
		if($this->track_cf7()) {
			$data = ['cf7string' => $this->cf7string];
			$this->load_template('cf7',$data);
		}

		if( class_exists('woocommerce') ) {

			if( is_order_received_page() ) {
				$details = $this->get_order_details();
				$details['send_to'] = $google_ads_id . "/" . Helper::get_theme_option('conversion_id');
				$this->load_template('googleads', $details);
			}

		}
	}

	protected function print_facebook_pixel()
	{
		if( !$this->check_consent() ) return;

		if(Helper::get_theme_option('gai_enable_facebook_pixel') && Helper::get_theme_option('gai_facebook_pixel')) {
			$data = ['pixel' => Helper::get_theme_option('gai_facebook_pixel')];
			$this->load_template('fb',$data);
		}

	}

	protected function print_umami()
	{
		if(Helper::get_theme_option('gai_enable_umami'))
		{
			$data = [
				'site_id' 	=> Helper::get_theme_option('gai_umami_site_id'),
				'umami_url'	=> Helper::get_theme_option('gai_umami_script_url')
			];

			$this->load_template('umami',$data);
		}
	}

	protected function check_consent()
	{
		if(function_exists('cn_cookies_accepted') && Helper::get_theme_option('gai_consent_integration')) {
			return cn_cookies_accepted();
		}

		return true;
	}

	protected function set_cf7string()
	{
		if($this->track_cf7() && $this->post_id) {
			$post = get_post();
			preg_match("/\[contact-form-7(.+?)?\]/",$post->post_content,$matches);
			if(isset($matches[1])) {
				$atts = shortcode_parse_atts($matches[1]);
				if(isset($atts['title'])) {
					$this->cf7string = $atts['title'];
				}
			}
		}
	}

	protected function track_cf7()
	{
		if(defined("WPCF7_VERSION") && Helper::get_theme_option('gai_track_cf7')){
			return true;
		}
		return false;
	}

	protected function get_google_ads_id()
	{
		$id = Helper::get_theme_option('google_ads_id');
		return  ($id === $this->ga_tracking_id || empty($id))?false:$id;
	}

	protected function get_order_details()
	{
		global $wp;
		
		$order = new \WC_Order(absint( $wp->query_vars['order-received'] ));;

		return [
			'value' 			=> $order->get_total(),
			'currency'			=> $order->get_currency(),
			'transaction_id'	=> $order->get_id()
		];
	}

}
